﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Gun : MonoBehaviourPunCallbacks
{
    public Transform gunTransform;
    public ParticleSystem ps;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //shoot
                photonView.RPC("RPC_Shoot", RpcTarget.All);
            }
        }
    }


    //get called on all the instances of the viewID
    [PunRPC]
    void RPC_Shoot()
    {
        //Shoot
        ps.Play();
        Ray ray = new Ray(gunTransform.position, gunTransform.forward);
        if (Physics.Raycast(ray, out RaycastHit hit, 100f))
        {
            //We hit something
            //check git has heatlh script we can  take some value off health.
        }
    }
}